using DPP4Coresets
#This if for Fig. 6

cd("/path/to/codes_for_some_figures")
###==== study DPP vs matched iid
    n_real = 1000
    study_DPP_vs_matched_iid(n_real)
    study_DPP_vs_matched_iid_lr(n_real)
###==== plot results

using Plots
using StatsBase
using JLD
plotly() # or whichever backend you prefer

epsilon = 0.1

#dic = load("results/study_DPP_vs_iid_d=2.jld")
dic = load("results/study_DPP_vs_iid_d=2_lr.jld")
ERR = dic["ERR"];
m_max = dic["m_max"];
τ1 = dic["τ1"]
τ2 = dic["τ2"]
n_theta = dic["n_theta"]

P_success = sum(ERR .< epsilon, dims=3)/n_theta; P_success = P_success[:,:,1,:]
P_success_m = StatsBase.mean(P_success, dims=2); P_success_m = P_success_m[:,1,:]

label_str = ["m-DPP (τ = $(τ1))" "m-DPP (τ = $(τ2))" "Proj Poly DPP" "matched iid (τ = $(τ1))" "matched iid (τ = $(τ2))" "matched iid (Poly)" "sensitivity iid"]

# Figure importance sampling
begin
    plot(1:m_max, P_success_m[5,:], line=(:dot,4), color=[3],label=label_str[7])
    m_Vdm = findall(P_success_m[end,:] .!= 0)
    plot!(1:m_max, P_success_m[1,:], line=(:solid,4), color = [1], label=label_str[1])
    #plot!(1:m_max, P_success_m[3,:], line=(:solid,4, :blue), label=label_str[2])
    plot!(m_Vdm, P_success_m[6,m_Vdm], line=(:solid,4), color = [5], m=(8, :auto), label=label_str[3])
    plot!(1:m_max, P_success_m[2,:], line=(:dash,4), color = [1], label=label_str[4])
    #plot!(1:m_max, P_success_m[4,:], line=(:dash,4, :blue), label=label_str[5])
    plot!(m_Vdm, P_success_m[7,m_Vdm], line=(:dash,4), color = [5], m=(8, :auto), label=label_str[6])

    plot!(framestyle=:zerolines, xlabel = "number of samples m", ylabel = "P(ΔL < εL)")
    plot!(legendfontsize=12, xlims = (0,m_max),ylims = (0,1))
    plot!(yticks = [0,0.5,1], tickfontsize=16)
    plot!(guidefontsize=16)
end

using DPP
using RCall
cd("/path/to/codes_for_some_figures")

μ = [[1, 1],[-1,-1],[1,-1]]
ns = [2000,2000,20]
X = mapreduce((v) -> .2*randn(length(v[1]),v[2]) .+ v[1],hcat,zip(μ,ns))
L = polyfeatures(X,6) |> ProjectionEnsemble #nb: rank = 28
ii = DPP.sample(L) |> collect
R"""
pdf(\"results/illus_rebalancing_clusters.pdf\",w=5,h=5)
plot(t($X),pch=19,col=rgb(.5,.5,.5,.2),xlab=expression(x[1]),ylab=expression(x[2]))
points(t($X)[$ii,],pch=19,col=rgb(0,0,.5,.9))
dev.off()
"""

#sample from the ball in R^d. If γ=-1.0, sample uniformly
function sample_ball(n :: Int,d :: Int,γ=-1.0)
    X = randn(d,n)
    X = X ./ sqrt.(sum( X .^ 2,dims=1))
    r = rand(n).^(d^γ)
    r' .* X
end
n = 12000
X1 = sample_ball(n,2)
L1 = polyfeatures(X1,12) |> ProjectionEnsemble
ii1 = DPP.sample(L1) |> collect

X2 = sample_ball(n,2,.5)
L2 = polyfeatures(X2,12) |> ProjectionEnsemble
ii2 = DPP.sample(L2) |> collect
R"""
library(tidyverse)
library(cowplot)
n = $n
df = data.frame(x1=c($X1[1,],$X2[1,]),x2=c($X1[2,],$X2[2,]),
distr=gl(2,n),incl=c(1:n %in% $ii1,1:n %in% $ii2))
df = arrange(df,incl)
"""
R"""
p = ggplot(df,aes(x1,x2))+geom_point(aes(col=incl))+facet_wrap(~ distr)+scale_colour_manual(values=c(rgb(.5,.5,.5,.2),rgb(0,0,.5,.9)))+theme(legend.pos=\"none\")+labs(x=expression(x[1]),y=expression(x[2]))
ggsave('results/illus_rebalancing_discs_1.pdf',p,w=8,h=4)
"""

Y1 = reduce(hcat,[X1[:,collect(DPP.sample(L1))] for _ in 1:30])
Y2 = reduce(hcat,[X2[:,collect(DPP.sample(L2))] for _ in 1:30])

R"""
df = data.frame(x1=c($Y1[1,],$Y2[1,]),x2=c($Y1[2,],$Y2[2,]),
distr=gl(2,ncol($Y1)))
p=ggplot(df,aes(x1,x2))+geom_point(col=rgb(0,0,.5,.9))+facet_wrap(~ distr)+theme(legend.pos=\"none\")+labs(x=expression(x[1]),y=expression(x[2]))
ggsave('results/illus_rebalancing_discs_2.pdf',p,w=8,h=4)
"""


#X = mapreduce((m,n) -> randn(length(m),ns) .+ m,hcat,zip(μ,ns))

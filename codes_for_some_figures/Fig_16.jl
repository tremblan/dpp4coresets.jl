using DPP4Coresets
#This is for Fig_16

cd("/path/to/codes_for_some_figures")
n_theta = 1000
n_real = 200
study_USCensus(n_theta, n_real)

##== plot results

using Plots, StatsBase, JLD
plotly()
epsilon=0.1
n_real = 200
dic = load("results/US_Census_nn=$n_real.jld")
ERR = dic["ERR"]
ERR_V = dic["ERR_V"]
m_max = dic["m_max"]
n_comp = dic["n_comp"]
n_theta = dic["n_theta"]

ERR = ERR[:,1:n_real,:,:]
ERR_V = ERR_V[:,1:n_real,:,:]

P_success = sum(ERR .< epsilon, dims=3)/n_theta; P_success = P_success[:,:,1,:]
P_success_V = sum(ERR_V .< epsilon, dims=3)/n_theta; P_success_V = P_success_V[:,:,1,:]
P_success_m = StatsBase.mean(P_success, dims=2); P_success_m = P_success_m[:,1,:]
P_success_V_m = StatsBase.mean(P_success_V, dims=2); P_success_V_m = P_success_V_m[:,1,:]

#figure weighted sampling
begin
    label_str = ["m-DPP (τ=70)" "uniform" "sensitivity iid"]

    plot(1:m_max, P_success_m[1:3,:]', line=(:auto,4), label=label_str)

    plot!(framestyle=:zerolines, xlabel = "number of samples m", ylabel = "P(ΔL < εL)")
    plot!(legendfontsize=16, xlims = (0,m_max),ylims = (0,1))
    plot!(yticks = [0,0.5,1], tickfontsize=16, guidefontsize=16)
end

# Figure Voronoi
begin
    label_str = ["m-DPP (τ=70)" "uniform" "sensitivity iid" "D^2"]

    plot(1:m_max, P_success_V_m', line=(:auto,4), label=label_str)

    plot!(framestyle=:zerolines, xlabel = "number of samples m", ylabel = "P(ΔL < εL)")
    plot!(legendfontsize=16, xlims = (0,m_max),ylims = (0,1))
    plot!(yticks = [0,0.5,1], tickfontsize=16, guidefontsize=16)
end

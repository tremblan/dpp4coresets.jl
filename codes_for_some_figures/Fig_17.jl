using DPP4Coresets
cd("/path/to/codes_for_some_figures")
# you need to unzip the zip file in the /data folder

###========================

N = [1000, 10000, 50000, 100000]
M = [3, 6, 10, 15, 21, 28]
D = [2, 20, 50, 100]

study_computation_time(N, D, M)
##==========================

using Plots
using StatsBase
using JLD
plotly() # or whichever backend you prefer

dic = load("results/computation_times_d=100_n=100000.jld")
time_rff = dic["time_rff"]
time_Lr = dic["time_Lr"]
time_sample_mDPP = dic["time_sample_mDPP"]
time_pi_mDPP = dic["time_pi_mDPP"]
time_sample_unif = dic["time_sample_unif"]
time_get_sensi = dic["time_get_sensi"]
time_sample_sensi = dic["time_sample_sensi"]
time_sample_Dsquare = dic["time_sample_Dsquare"]
time_compute_VdM = dic["time_compute_VdM"]
time_compute_polyfeatures = dic["time_compute_polyfeatures"]
time_sample_polyfeatures = dic["time_sample_polyfeatures"]
time_get_pi_polyfeatures = dic["time_get_pi_polyfeatures"]
N = dic["N"]; D = dic["D"]; M = dic["M"]

# the following 9 Figures are in Fig.17 of the paper:
# Figure: mDPP_rff vs m
begin
    nn=4; #n=100000
    dd=1; #d=2
    Plots.plot(M, time_rff[nn, dd, :], line=(:auto,4), color=:darkblue, label="compute rff")
    Plots.plot!(M, time_Lr[nn, dd, :], line=(:auto,4), color=:darkblue, label="SVD of rff")
    Plots.plot!(M, time_sample_mDPP[nn, dd, :],  line=(:auto,4), color=:darkblue, label="sample")
    Plots.plot!(M, time_pi_mDPP[nn, dd, :],  line=(:auto,4), color=:darkblue, label="compute π")
    xaxis!(xlims=[3, 28])
    xaxis!(ylims=[0.00000005, 0.2])
    plot!(framestyle=:zerolines, xlabel = "m", ylabel = "time (s)")
    plot!(legendfontsize=20)
    plot!(tickfontsize=12, guidefontsize=20)
    #yaxis!(:log10)
    #xaxis!(:log10)
end

# Figure: PolyProj vs m
begin
    nn=4; #n=100000
    dd=1; #d=2
    m_Vdm = findall(isnan.(time_compute_polyfeatures[nn, dd, :]) .== false)
    Plots.plot(M[m_Vdm], time_compute_VdM[nn, dd, m_Vdm],  line=(:auto,4), color=:green, label="compute Poly")
    Plots.plot!(M[m_Vdm], time_compute_polyfeatures[nn, dd, m_Vdm], color=:green,  line=(:auto,4), label="QR")
    Plots.plot!(M[m_Vdm], time_sample_polyfeatures[nn, dd, m_Vdm], color=:green,  line=(:auto,4), label="sample")
    Plots.plot!(M[m_Vdm], time_get_pi_polyfeatures[nn, dd, m_Vdm], color=:green,  line=(:auto,4), label="compute π")
    xaxis!(xlims=[3, 28])
    xaxis!(ylims=[0.00000005, 0.2])
    plot!(framestyle=:zerolines, xlabel = "m", ylabel = "time (s)")
    plot!(legendfontsize=20)
    plot!(tickfontsize=12, guidefontsize=20)
    #yaxis!(:log10)
    #xaxis!(:log10)
end

# Figure: iid vs m
begin
    nn=4; #n=100000
    dd=1; #d=2
    Plots.plot(M, time_get_sensi[nn, dd, :],  line=(:auto,4), color=:red, label="compute sensi")
    Plots.plot!(M, time_sample_sensi[nn, dd, :],  line=(:auto,4), color=:red, label="sample sensi iid")
    Plots.plot!(M, time_sample_unif[nn, dd, :],  line=(:auto,4), label="sample unif iid")
    Plots.plot!(M, time_sample_Dsquare[nn, dd, :],  line=(:auto,4), label="sample D^2")
    xaxis!(xlims=[3, 28])
    xaxis!(ylims=[0.00000005, 0.2])
    plot!(framestyle=:zerolines, xlabel = "m", ylabel = "time (s)")
    plot!(legendfontsize=20)
    plot!(tickfontsize=12, guidefontsize=20)
    #yaxis!(:log10)
    #xaxis!(:log10)
end



# Figure: mDPP_rff vs n
begin
    mm=3; # corresponds to m=10
    dd=1; # correspons to d=2
    Plots.plot(N, time_rff[:, dd, mm], line=(:auto,4), color=:darkblue, label="Compute rff")
    Plots.plot!(N, time_Lr[:, dd, mm], line=(:auto,4), color=:darkblue, label="SVD of rff")
    Plots.plot!(N, time_sample_mDPP[:, dd, mm],  line=(:auto,4), color=:darkblue, label="sample")
    Plots.plot!(N, time_pi_mDPP[:, dd, mm],  line=(:auto,4), color=:darkblue, label="compute π")
    xaxis!(xlims=[1000, 100000])
    xaxis!(ylims=[0.00000005, 0.1])
    plot!(framestyle=:zerolines, xlabel = "n", ylabel = "time (s)")
    plot!(legendfontsize=20)
    plot!(tickfontsize=12, guidefontsize=20)
    #yaxis!(:log10)
    #xaxis!(:log10)
end

# Figure: PolyProj vs n
begin
    mm=3; # corresponds to m=10
    dd=1; # correspons to d=2
    n_Vdm = findall(isnan.(time_compute_polyfeatures[:, dd, mm]) .== false)
    Plots.plot(N[n_Vdm], time_compute_VdM[n_Vdm, dd, mm],  line=(:auto,4), color=:green, label="compute Poly")
    Plots.plot!(N[n_Vdm], time_compute_polyfeatures[n_Vdm, dd, mm],  line=(:auto,4), color=:green, label="QR")
    Plots.plot!(N[n_Vdm], time_sample_polyfeatures[n_Vdm, dd, mm],  line=(:auto,4), color=:green, label="sample")
    Plots.plot!(N[n_Vdm], time_get_pi_polyfeatures[n_Vdm, dd, mm],  line=(:auto,4), color=:green, label="compute π")
    xaxis!(xlims=[1000, 100000])
    xaxis!(ylims=[0.00000005, 0.1])
    plot!(framestyle=:zerolines, xlabel = "n", ylabel = "time (s)")
    plot!(legendfontsize=20)
    plot!(tickfontsize=12, guidefontsize=20)
    #yaxis!(:log10)
    #xaxis!(:log10)
end

# Figure: iid vs n
begin
    mm=3; # corresponds to m=10
    dd=1; # correspons to d=2
    Plots.plot(N, time_get_sensi[:, dd, mm],  line=(:auto,4), color=:red, label="compute sensi")
    Plots.plot!(N, time_sample_sensi[:, dd, mm],  line=(:auto,4), color=:red, label="sample sensi iid")
    Plots.plot!(N, time_sample_unif[:, dd, mm],  line=(:auto,4), label="sample unif iid")
    Plots.plot!(N, time_sample_Dsquare[:, dd, mm],  line=(:auto,4), label="sample D^2")
    xaxis!(xlims=[1000, 100000])
    xaxis!(ylims=[0.00000005, 0.1])
    plot!(framestyle=:zerolines, xlabel = "n", ylabel = "time (s)")
    plot!(legendfontsize=20)
    plot!(tickfontsize=12, guidefontsize=20)
    #yaxis!(:log10)
    #xaxis!(:log10)
end


# Figure: mDPP_rff vs d
begin
    mm=5; # corresponds to m=21
    nn=4; # correspons to n=100000
    Plots.plot(D, time_rff[nn, :, mm], line=(:auto,4), color=:darkblue, label="Compute rff")
    Plots.plot!(D, time_Lr[nn, :, mm], line=(:auto,4), color=:darkblue, label="SVD of rff")
    Plots.plot!(D, time_sample_mDPP[nn, :, mm],  line=(:auto,4), color=:darkblue, label="sample")
    Plots.plot!(D, time_pi_mDPP[nn, :, mm],  line=(:auto,4), color=:darkblue, label="compute π")
    xaxis!(xlims=[2, 100])
    xaxis!(ylims=[0.00000005, 0.4])
    plot!(framestyle=:zerolines, xlabel = "d", ylabel = "time (s)")
    plot!(legendfontsize=20)
    plot!(tickfontsize=12, guidefontsize=20)
    #yaxis!(:log10)
    #xaxis!(:log10)
end

# Figure: PolyProj vs d
begin
    mm=5; # corresponds to m=21
    nn=4; # correspons to n=100000
    d_Vdm = findall(isnan.(time_compute_polyfeatures[nn, :, mm]) .== false)
    Plots.plot(D[d_Vdm], time_compute_VdM[nn, d_Vdm, mm],  line=(:auto,4), color=:green, label="compute Poly")
    Plots.plot!(D[d_Vdm], time_compute_polyfeatures[nn, d_Vdm, mm],  line=(:auto,4), color=:green, label="QR")
    Plots.plot!(D[d_Vdm], time_sample_polyfeatures[nn, d_Vdm, mm],  line=(:auto,4), color=:green, label="sample")
    Plots.plot!(D[d_Vdm], time_get_pi_polyfeatures[nn, d_Vdm, mm],  line=(:auto,4), color=:green, label="compute π")
    xaxis!(xlims=[2, 100])
    xaxis!(ylims=[0.00000005, 0.4])
    plot!(framestyle=:zerolines, xlabel = "d", ylabel = "time (s)")
    plot!(legendfontsize=20)
    plot!(tickfontsize=12, guidefontsize=20)
    #yaxis!(:log10)
    #xaxis!(:log10)
end

# Figure: iid vs d
begin
    mm=5; # corresponds to m=21
    nn=4; # correspons to n=100000
    Plots.plot(D, time_get_sensi[nn, :, mm],  line=(:auto,4), color=:red, label="compute sensi")
    Plots.plot!(D, time_sample_sensi[nn, :, mm],  line=(:auto,4), color=:red, label="sample sensi iid")
    Plots.plot!(D, time_sample_unif[nn, :, mm],  line=(:auto,4), label="sample unif iid")
    Plots.plot!(D, time_sample_Dsquare[nn, :, mm],  line=(:auto,4), label="sample D^2")
    xaxis!(xlims=[2, 100])
    xaxis!(ylims=[0.00000005, 0.4])
    plot!(framestyle=:zerolines, xlabel = "d", ylabel = "time (s)")
    plot!(legendfontsize=20)
    plot!(tickfontsize=12, guidefontsize=20)
    #yaxis!(:log10)
    #xaxis!(:log10)
end

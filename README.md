This is the DPP4Coreset box, a Julia toolbox that implements the DPP-based sampling algorithm for coresets as detailed in  

    N. Tremblay, S. Barthelmé, Pierre-Olivier Amblard.
    Determinantal Point Processes for Coresets.
    arxiv.org/pdf/1803.08700.pdf
    http://jmlr.org/papers/volume20/18-167/18-167.pdf

The authors of this Toolbox are Nicolas Tremblay and Simon Barthelmé and can be joined by email at firstname.lastname ATT grenoble-inp DOT fr

This toolbox is probably not bug-free and this is its BETA-version. Please report any bug to help us improve it. 

This Toolbox is under the GNU Affero Public License (see the attached document "LICENSE"). 

If you use this Toolbox, please kindly cite us! :-)

========= NOTA BENE BEFORE YOU USE THE TOOLBOX =========

* This toolbox uses the Julia DPP toolbox (that we wrote for the occasion). Before installing DPP4coreset, you need to install the DPP package by typing (in the pkg manager):
'''add https://gricad-gitlab.univ-grenoble-alpes.fr/barthesi/dpp.jl'''
You can then install this toolbox by either typing (in the pkg manager)
'''add https://gricad-gitlab.univ-grenoble-alpes.fr/tremblan/dpp4coresets.jl'''
or cloning the repo locally and typing (in the pkg manager) '''add dpp4coresets.jl'''

* Unzip the zip file in the folder /data

* from the demo file, you should be able to understand how to sample from a dataset using several different methods: m-DPP with the exact Gaussian kernel, m-DPP using RFFs, Polynomial Projective DPP, uniform iid, sensitivity iid (either exact in the 1-means or linear regression cases, or upper bounds in the k-means case), and finally the D^2 method.

* we reproduce in the folder src_for_figures the exact codes used to create the figures in the paper. Some of them are long to run. Some figures of the paper (such as the figures using the Stochastic Block Model) were created by old Python codes. If you need to see the source, please contact us.

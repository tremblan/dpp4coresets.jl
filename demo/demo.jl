using JLD, DPP4Coresets, Plots
plotly() # or whichever backend you prefer
# you have to unzip to .zip file in the /data folder

# ===================================
# ============= Data ================
# ===================================

# === examples of artificial data ===
# ====== Mixture of Gaussians =======
begin
    n = 1000 # number of points
    d = 2; # dimension
    n_comp = 4; # number of Gaussians
    q = 0.01 # outlier percentage
    #size of each cluster:
    p_comp = Vector([0.25; 0.01; 0.54; 0.2])
    #mean of each cluster:
    μ = zeros(Float64, (n_comp, d)); μ[1,:] = [0 0];μ[2,:] = [10 0];μ[3,:] = [5 5];μ[4,:] = [-6 3]
    #covariance matrix of each cluster
    Σ = zeros(Float64, (n_comp, d, d));Σ[1,:,:] = [1.0 0; 0 1.2];Σ[2,:,:] = [1.0 0.0; 0.3 1.0];Σ[3,:,:] = [0.5 0; 0.5 2.0];Σ[4,:,:] = [1 0; 1.5 2.0]
    X, ground_truth = generate_GMM(μ, Σ, p_comp, n, q)
    Plots.scatter(X[1,:],X[2,:],color=ground_truth.+1,alpha=.5, label="")
end
# === A linear regression problem ===
begin
    n = 1000
    σ_noise = 0.1
    d = 1;
    X_0, y, theta_gt = generate_lr(d,n,σ_noise)
    Plots.scatter(X_0',y,alpha=.5, label="")
    X = [X_0;y'] #(called Z in the paper)
end

# === examples of real-world data ===
# =========== US Census =============
begin
    dic = load("data/USCensus_first_column_removed.jld")
    X = dic["X"]
end
# == or MNIST's spectral features ===
begin
    dic = load("data/MNIST_spectral_features.jld")
    X = dic["X"]
    dic = load("data/MNIST_truth.jld")
    ground_truth = dic["ground_truth"]
    # set τ to 1.5 for instance in the following Gaussian kernels
end

# ===================================
# ============ Sample ===============
# ===================================

m = 15; # number of desired samples

# = different sampling strategies: ==
# ===================================
# 1.=== m-DPP with true kernel ======
# ========= (not in paper) ==========
    method = "m-DPP_true_kernel";
    τ = 5.; # the Gaussian parameter (should be a Float)
    r = -1; problem = "N/A" #useless parameters here
# 2.======= m-DPP with RFFs =========
# ======== (mDPP in paper) ==========
    method = "m-DPP_rff";
    τ = 5.; # the Gaussian parameter (should be a Float)
    r = 2*m; # the number of RFFs
    problem = "N/A" #useless parameters here
# 3.=== Projective Polynomial DPP ===
# ====== (PolyProj-DPP in paper) ====
    method = "VdM_projective";
    τ = -1.; r = -1; problem = "N/A" #useless parameters here
# 4.========== uniform iid ==========
# ====== (uniform iid in paper) =====
    method = "uniform";
    τ = -1.; r = -1; problem = "N/A" #useless parameters here
# 5.====== sensitivity iid ==========
# === (sensitivity iid in paper) ====
# here with the exact sensitivity (if 1-means or linear regression)
    method = "exact_sensitivity"; problem = "1-means"; # if 1 means
    method = "exact_sensitivity"; problem = "linear-regression";  # if linear regression
    τ = -1.; r = -1; #useless parameters here
# 6.== D-square (D^2 in paper) ======
    method = "D-square";
    τ = -1.; r = -1; problem = "N/A" #useless parameters here

# Once the method is chosen, run the sampling algorithm:
    ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
# ind is the sample (vector of size m)
# pi is the proba of inclusion of all elements (vector of size n)
# ind_iid is a sample (vector of size m) obtained by sampling iid with replacement from pi/sum(pi) [returns NaN if the method is already iid]

# an exception:
# in the kmeans context, one can sample with the bi-criteria
# approx of the sensitivity (only if k-means):
    method = "approx_sensitivity";
    k = 4 # the number of classes
    τ = -1.; r = -1; problem = "N/A" #useless parameters here
    ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem, k)

# ===================================
# ============= Plot ================
# ===================================

# if Gaussian Mixture in dimension 2:
begin
    # On this plot the original points are in transparency,
    # the sampled ones in full red
    Plots.scatter(X[1,:],X[2,:],color=ground_truth.+1,alpha=.5, label="")
    marksize = 1 ./ pi[ind]; marksize = (6 .* marksize ./ maximum(marksize)) .+ 3
    Plots.scatter!(X[1,ind],X[2,ind],color=:red,alpha=1,label="samples", markersize=marksize)
end

# if linear regression problem:
begin
    # On this plot the original points are in transparency,
    # the sampled ones in full red
    Plots.scatter(X_0',y,alpha=.5, label="")
    marksize = 1 ./ pi[ind]; marksize = (6 .* marksize ./ maximum(marksize)) .+ 3
    Plots.scatter!(X_0[ind],y[ind],color=:red,alpha=1,label="samples", markersize=marksize)
end

# if data is MNIST:
begin
    count_in_classes = zeros(Int64, 10)
    aux = ground_truth[ind]
    for i=0:9
        count_in_classes[i+1] = sum(aux .== i)
    end
    Plots.bar(0:9, count_in_classes)
    Plots.plot!(xticks = 0:9, xlabel = "MNIST class number", ylabel = "# of times the class was sampled")
end

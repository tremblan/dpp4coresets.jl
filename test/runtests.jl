#basic structure copied from LightGraphs.jl
using DPP4Coresets
using Test

const testdir = dirname(@__FILE__)
tests = [
    "tests"
]

@testset "DPP" begin
    for t in tests
        tp = joinpath(testdir, "$(t).jl")
        include(tp)
    end
end

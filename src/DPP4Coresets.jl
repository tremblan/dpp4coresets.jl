module DPP4Coresets

export study_computation_time, study_DPP_vs_matched_iid_lr, study_DPP_vs_matched_iid, study_USCensus, routine_1_means_vs_S, routine_lr_vs_S, routine_lr, routine_1_means, generate_GMM, sample_for_coresets, compute_km_cost, Voronoi_weights, generate_lr, test_lr_coreset, test_km_coreset, compute_lr_cost

using DPP
using NearestNeighbors, LinearAlgebra, StatsBase, Distances, JLD, BenchmarkTools

"""
    routine_1_means_vs_S(n_real)

A routine function for Fig. 4, to test method m-DPP with RFFs sor different values of τ

    ## Inputs
        n_real the number of realizations of the routine

    ## Outputs
        saved files
"""
function routine_1_means_vs_S(n_real)
    m_max = 30
    n = 1000
    q = 0.
    n_theta = 50
    n_comp = 1;
    p_comp = Vector([1.])

    d=2
    μ = zeros(Float64, (n_comp, d)); μ[1,:] = zeros(Float64, d)
    Σ = zeros(Float64, (n_comp, d, d)); Σ[1,:,:] = Matrix{Float64}(I, d, d)
    S = [0.12, 0.25, 0.5, 0.75, 1., 1.5]
    ERR, ERR_V = test_km_coreset_vs_S(n_real, n_theta, n_comp, m_max, μ, Σ, p_comp, n, q, S)
    save("results/study_1_means_vs_S_d=2.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "S", S, "n_theta", n_theta)

    d=20
    μ = zeros(Float64, (n_comp, d)); μ[1,:] = zeros(Float64, d)
    Σ = zeros(Float64, (n_comp, d, d));Σ[1,:,:] = Matrix{Float64}(I, d, d)
    S = [1., 2., 4., 6., 14.]
    ERR, ERR_V = test_km_coreset_vs_S(n_real, n_theta, n_comp, m_max, μ, Σ, p_comp, n, q, S)
    save("results/study_1_means_vs_S_d=20.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "S", S, "n_theta", n_theta)

    d=100
    μ = zeros(Float64, (n_comp, d)); μ[1,:] = zeros(Float64, d)
    Σ = zeros(Float64, (n_comp, d, d));Σ[1,:,:] = Matrix{Float64}(I, d, d)
    S = [1., 7., 10., 20.]
    ERR, ERR_V = test_km_coreset_vs_S(n_real, n_theta, n_comp, m_max, μ, Σ, p_comp, n, q, S)
    save("results/study_1_means_vs_S_d=100.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "S", S, "n_theta", n_theta)
end

"""
    routine_1_means(n_real)

A routine function for Fig. 5: compare methods for coresets for 1 means.

    ## Inputs
        n_real the number of realizations of the routine

    ## Outputs
        saved files
"""
function routine_1_means(n_real)

    # ========== parameters for 1 Gaussian
    n_comp = 1;
    p_comp = Vector([1.])
    # ==

    n = 1000
    q = 0.
    m_max = 30
    n_theta = 50

    d = 2;
    μ = zeros(Float64, (n_comp, d)); μ[1,:] = zeros(Float64, d)
    Σ = zeros(Float64, (n_comp, d, d));Σ[1,:,:] = Matrix{Float64}(I, d, d)
    τ1 = 1.; τ2 = 1.5;
    ERR, ERR_V = test_km_coreset(n_real, n_theta, n_comp, m_max, μ, Σ, p_comp, n, q, τ1, τ2)
    save("results/1means_d=2.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "τ1", τ1, "τ2", τ2, "n_theta", n_theta)

    d = 20;
    μ = zeros(Float64, (n_comp, d)); μ[1,:] = zeros(Float64, d)
    Σ = zeros(Float64, (n_comp, d, d));Σ[1,:,:] = Matrix{Float64}(I, d, d)
    τ1 = 4.; τ2 = 6.
    ERR, ERR_V = test_km_coreset(n_real, n_theta, n_comp, m_max, μ, Σ, p_comp, n, q, τ1, τ2)
    save("results/1means_d=20.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "τ1", τ1, "τ2", τ2, "n_theta", n_theta)

    d = 100;
    μ = zeros(Float64, (n_comp, d)); μ[1,:] = zeros(Float64, d)
    Σ = zeros(Float64, (n_comp, d, d));Σ[1,:,:] = Matrix{Float64}(I, d, d)
    τ1 = 7.; τ2 = 10.
    ERR, ERR_V = test_km_coreset(n_real, n_theta, n_comp, m_max, μ, Σ, p_comp, n, q, τ1, τ2)
    save("results/1means_d=100.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "τ1", τ1, "τ2", τ2, "n_theta", n_theta)
end

"""
    routine_lr_vs_S(n_real)

A routine function for Fig. 9: , to test method m-DPP with RFFs for different values of τ, in the linear regression setting

    ## Inputs
        n_real the number of realizations of the routine

    ## Outputs
        saved files
"""
function routine_lr_vs_S(n_real)
    n = 1000
    σ_noise = 0.1
    n_theta = 50
    m_max = 30

    d = 2;
    S = [0.02, 0.04, 0.08, 0.15, 0.3, 0.45, 0.6, 0.75]
    ERR, ERR_V = test_lr_coreset_vs_S(n_real, n_theta, m_max, d, n, σ_noise, S)
    save("results/study_lr_vs_S_d=2_V_Z.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "S", S, "n_theta", n_theta)

    d = 20;
    S = [1., 2., 4., 6., 10.]
    ERR, ERR_V = test_lr_coreset_vs_S(n_real, n_theta, m_max, d, n, σ_noise, S)
    save("results/study_lr_vs_S_d=20_V_Z.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "S", S, "n_theta", n_theta)

    d = 100;
    S = [1., 2., 3., 5., 7., 10., 20.]
    ERR, ERR_V = test_lr_coreset_vs_S(n_real, n_theta, m_max, d, n, σ_noise, S)
    save("results/study_lr_vs_S_d=100_V_Z.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "S", S, "n_theta", n_theta)
end

"""
    routine_lr_vs(n_real)

A routine function for Fig. 10: compare methods for coresets for linear regression.

    ## Inputs
        n_real the number of realizations of the routine

    ## Outputs
        saved files
"""
function routine_lr(n_real)
    n = 1000
    m_max = 30
    σ_noise = 0.1
    n_theta = 50

    d = 2; τ1 = 0.3; τ2 = 0.45
    ERR, ERR_V = test_lr_coreset(n_real, n_theta, m_max, d, n, σ_noise,τ1,τ2)
    save("results/lr_d=2_V_Z.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "τ1", τ1, "τ2", τ2, "n_theta", n_theta)

    d = 20; τ1 = 4.; τ2 = 6.
    ERR, ERR_V = test_lr_coreset(n_real, n_theta, m_max, d, n, σ_noise,τ1,τ2)
    save("results/lr_d=20_V_Z.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "τ1", τ1, "τ2", τ2, "n_theta", n_theta)

    d = 100; τ1 = 7.; τ2 = 10.
    ERR, ERR_V = test_lr_coreset(n_real, n_theta, m_max, d, n, σ_noise,τ1,τ2)
    save("results/lr_d=100_V_Z.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "τ1", τ1, "τ2", τ2, "n_theta", n_theta)
end

"""
    study_DPP_vs_matched_iid(n_real)

A routine function for Fig. 6: compare DPP methods with their matched iid, in the 1-means context

    ## Inputs
        n_real the number of realizations of the routine

    ## Outputs
        saved files
"""
function study_DPP_vs_matched_iid(n_real)
    m_max = 30
    n = 1000
    q = 0.
    n_theta = 50
    n_comp = 1;
    p_comp = Vector([1.])

    d=2
    μ = zeros(Float64, (n_comp, d)); μ[1,:] = zeros(Float64, d)
    Σ = zeros(Float64, (n_comp, d, d)); Σ[1,:,:] = Matrix{Float64}(I, d, d)
    τ1 = .75
    τ2 = 1.

    n_methods = 7
    ERR = zeros(Float64, (n_methods, n_real, n_theta, m_max))

    for i=1:n_real
        print("$i \n")
        X, ground_truth = generate_GMM(μ, Σ, p_comp, n, q)
        thetas = X[:,rand(1:n, n_theta * n_comp)]
        true_cost = compute_km_cost(X, thetas, n_comp, n_theta)
        for m=1:m_max
            method = "m-DPP_rff"; τ=τ1; r = 200; problem = "N/A"
            ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
            if isnan(pi[1])
                error("pi was not computed")
            end
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[1,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            est_cost = compute_km_cost(X[:, ind_iid], thetas, n_comp, n_theta, pi[ind_iid])
            ERR[2,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "m-DPP_rff"; τ=τ2; r = 200; problem = "N/A"
            ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
            if isnan(pi[1])
                error("pi was not computed")
            end
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[3,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            est_cost = compute_km_cost(X[:, ind_iid], thetas, n_comp, n_theta, pi[ind_iid])
            ERR[4,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "exact_sensitivity"; τ = -1.; r = -1; problem = "1-means"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[5,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "VdM_projective"; τ = -1.; r = -1; problem = "N/A"
            ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
            if !isnan(ind[1])
                est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
                ERR[6,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
                est_cost = compute_km_cost(X[:, ind_iid], thetas, n_comp, n_theta, pi[ind_iid])
                ERR[7,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            else
                ERR[6,i,:,m] = ones(Float64,n_theta) .* NaN
                ERR[7,i,:,m] = ones(Float64,n_theta) .* NaN
            end
        end
    end
    save("results/study_DPP_vs_iid_d=2.jld", "τ1", τ1, "τ2", τ2, "ERR", ERR, "m_max", m_max, "n_theta", n_theta)
end

"""
    study_DPP_vs_matched_iid_lr(n_real)

A routine function for Fig. 6: compare DPP methods with their matched iid, in the linear regression context

    ## Inputs
        n_real the number of realizations of the routine

    ## Outputs
        saved files
"""
function study_DPP_vs_matched_iid_lr(n_real)
    m_max = 30
    n = 1000
    q = 0.
    n_theta = 50

    d=2
    σ_noise = 0.1
    τ1 = .3
    τ2 = .45

    n_methods = 7
    ERR = zeros(Float64, (n_methods, n_real, n_theta, m_max))

    for i=1:n_real
        print("$i \n")
        X, y, theta_gt = generate_lr(d,n,σ_noise)
        thetas = rand(d, n_theta)
        true_cost = compute_lr_cost(X,y,thetas)
        Z = [X;y']
        for m=1:m_max
            method = "m-DPP_rff"; τ=τ1; r = 200; problem = "N/A"
            ind, pi, ind_iid = sample_for_coresets(method, Z, m, τ, r, problem)
            if isnan(pi[1])
                error("pi was not computed")
            end
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
            ERR[1,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            est_cost = compute_lr_cost(X[:,ind_iid],y[ind_iid],thetas,pi[ind_iid])
            ERR[2,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "m-DPP_rff"; τ=τ2; r = 200; problem = "N/A"
            ind, pi, ind_iid = sample_for_coresets(method, Z, m, τ, r, problem)
            if isnan(pi[1])
                error("pi was not computed")
            end
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
            ERR[3,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            est_cost = compute_lr_cost(X[:,ind_iid],y[ind_iid],thetas,pi[ind_iid])
            ERR[4,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "exact_sensitivity"; τ = -1.; r = -1; problem = "linear-regression"
            ind, pi, ~ = sample_for_coresets(method, Z, m, τ, r, problem)
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
            ERR[5,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "VdM_projective"; τ = -1.; r = -1; problem = "N/A"
            ind, pi, ind_iid = sample_for_coresets(method, Z, m, τ, r, problem)
            if !isnan(ind[1])
                est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
                ERR[6,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
                est_cost = compute_lr_cost(X[:,ind_iid],y[ind_iid],thetas,pi[ind_iid])
                ERR[7,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            else
                ERR[6,i,:,m] = ones(Float64,n_theta) .* NaN
                ERR[7,i,:,m] = ones(Float64,n_theta) .* NaN
            end
        end
    end
    save("results/study_DPP_vs_iid_d=2_lr.jld", "τ1", τ1, "τ2", τ2, "ERR", ERR, "m_max", m_max, "n_theta", n_theta)
end

"""
    study_USCensus(n_theta, n_real)

A routine function for Fig. 16: compare methods to find efficient coresets on the US Census dataset

    ## Inputs
        n_real the number of realizations of the routine

    ## Outputs
        saved files
"""
function study_USCensus(n_theta, n_real)
    dic = load("../data/USCensus_first_column_removed.jld")
    X = dic["X"]
    print("data loaded \n")

    (d, n) = size(X)
    n_comp = 15 #number of classes
    τ = 70. # mean Eucl dist between 1000 rand chosen col of X
    r = 30 #number of rff
    m_max = 30

    n_methods = 4
    ERR = zeros(Float64, (n_methods, n_real, n_theta, m_max))
    ERR_V = zeros(Float64, (n_methods, n_real, n_theta, m_max))

    thetas = X[:,rand(1:n, n_theta * n_comp)]
    true_cost = compute_km_cost(X, thetas, n_comp, n_theta)
    print("true costs computed for $n_theta realisations of $n_comp random centroids \n")
    for nn=1:n_real
        for m=1:m_max
            print("nn=$nn and m=$m \n")
            method = "m-DPP_rff"; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            if isnan(pi[1])
                error("pi was not computed")
            end
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[1,nn,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(X, X[:, ind])
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
            ERR_V[1,nn,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "uniform"; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[2,nn,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(X, X[:, ind])
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
            ERR_V[2,nn,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "approx_sensitivity"; problem = "k-means"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem, n_comp)
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[3,nn,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(X, X[:, ind])
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
            ERR_V[3,nn,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "D-square"; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            V_weights = Voronoi_weights(X, X[:, ind])
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
            ERR_V[4,nn,:,m] = abs.(true_cost - est_cost) ./ true_cost
            ERR[4,nn,:,m] = ones(Float64, n_theta) .* NaN
        end
        if isapprox(round(nn/50), nn/50)
            save("results/US_Census_nn=$nn.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "n_theta", n_theta, "n_comp", n_comp, "n_real", n_real, "true_cost", true_cost)
        end
    end
    save("results/US_Census.jld", "ERR", ERR, "ERR_V", ERR_V, "m_max", m_max, "n_theta", n_theta, "n_comp", n_comp, "n_real", n_real, "true_cost", true_cost)
end

"""
    study_computation_time(N, D, M)

A routine function for Fig. 17: compare computation times for different methods

    ## Inputs
        N a vector of different values of n
        D a vector of different values of d
        M a vector of different values of m

    ## Outputs
        saved files
"""
function study_computation_time(N, D, M)
    BenchmarkTools.DEFAULT_PARAMETERS.seconds = 10
    τ=1.; q = 0.
    time_rff = zeros(Float64, (length(N), length(D), length(M)))
    time_Lr = zeros(Float64, (length(N), length(D), length(M)))
    time_sample_mDPP = zeros(Float64, (length(N), length(D), length(M)))
    time_pi_mDPP = zeros(Float64, (length(N), length(D), length(M)))
    time_sample_unif = zeros(Float64, (length(N), length(D), length(M)))
    time_get_sensi = zeros(Float64, (length(N), length(D), length(M)))
    time_sample_sensi = zeros(Float64, (length(N), length(D), length(M)))
    time_sample_Dsquare = zeros(Float64, (length(N), length(D), length(M)))
    time_compute_VdM = zeros(Float64, (length(N), length(D), length(M)))
    time_compute_polyfeatures = zeros(Float64, (length(N), length(D), length(M)))
    time_sample_polyfeatures = zeros(Float64, (length(N), length(D), length(M)))
    time_get_pi_polyfeatures = zeros(Float64, (length(N), length(D), length(M)))

    for dd=1:length(D)
        # ========== parameters for 1 Gaussian
        d = D[dd]
        n_comp = 1;
        p_comp = Vector(ones(n_comp) ./ n_comp)
        if n_comp == 1
            μ = zeros(Float64, (n_comp, d)); μ[1,:] = zeros(Float64, d)
        else
            μ = rand(n_comp, d) *10;
        end
        Σ = zeros(Float64, (n_comp, d, d));
        for i=1:n_comp
            Σ[i,:,:] = Matrix{Float64}(I, d, d)
        end
        # ===============================
        for nn=1:length(N)
            n = N[nn]
            X, ground_truth = generate_GMM(μ, Σ, p_comp, n, q)

            for mm=1:length(M)
                m = M[mm]
                r = m
                print("d=$d, n=$n, m=$m \n")
                bench_rff = @benchmark rff($X, $r, $τ)
                time_rff[nn,dd,mm] = StatsBase.median(bench_rff.times)*10^(-9)
                rff_aux = rff(X, r, τ)
                bench_Lr = @benchmark $rff_aux |> LowRankEnsemble
                time_Lr[nn,dd,mm] = StatsBase.median(bench_Lr.times)*10^(-9)
                Lr = rff_aux |> LowRankEnsemble
                bench_sample = @benchmark DPP.sample($Lr, $m)
                time_sample_mDPP[nn,dd,mm] = StatsBase.median(bench_sample.times)*10^(-9)
                bench_pi = @benchmark inclusion_prob($Lr, $m)
                time_pi_mDPP[nn,dd,mm] = StatsBase.median(bench_pi.times)*10^(-9)
                bench_sample_unif = @benchmark StatsBase.sample($(1:n), $m; replace=true)
                time_sample_unif[nn,dd,mm] = StatsBase.median(bench_sample_unif.times)*10^(-9)
                bench_get_sensi = @benchmark get_approx_sensitivity_km($X, $n_comp)
                time_get_sensi[nn,dd,mm] = StatsBase.median(bench_get_sensi.times)*10^(-9)
                sensi = get_approx_sensitivity_km(X, n_comp)
                bench_sample_sensi = @benchmark StatsBase.sample($(1:n), $(StatsBase.weights(sensi./sum(sensi))), $m; replace=true)
                time_sample_sensi[nn,dd,mm] = StatsBase.median(bench_sample_sensi.times)*10^(-9)
                bench_sample_Dsquare = @benchmark D_square($X, $m)
                time_sample_Dsquare[nn,dd,mm] = StatsBase.median(bench_sample_Dsquare.times)*10^(-9)
                deg=0
                while binomial(deg+d, deg)<m
                    #global deg
                    deg += 1
                end
                if binomial(deg+d, deg) != m
                    #@warn "cannot sample with polynomials with these choices of d and m. The closest choices of m around m=$m are m=$(binomial(deg-1+d, deg-1)) or m=$(binomial(deg+d, deg))"
                    time_compute_VdM[nn,dd,mm] = NaN
                    time_compute_polyfeatures[nn,dd,mm] = NaN
                    time_sample_polyfeatures[nn,dd,mm] = NaN
                    time_get_pi_polyfeatures[nn,dd,mm] = NaN
                else
                    bench_compute_VdM = @benchmark polyfeatures($X, $deg)
                    time_compute_VdM[nn,dd,mm] = StatsBase.median(bench_compute_VdM.times)*10^(-9)
                    Vdm_aux = polyfeatures(X, deg)
                    bench_compute_polyfeatures = @benchmark $Vdm_aux |> ProjectionEnsemble
                    time_compute_polyfeatures[nn,dd,mm] = StatsBase.median(bench_compute_polyfeatures.times)*10^(-9)
                    Lp = Vdm_aux |> ProjectionEnsemble
                    bench_sample_polyfeatures = @benchmark DPP.sample($Lp)
                    time_sample_polyfeatures[nn,dd,mm] = StatsBase.median(bench_sample_polyfeatures.times)*10^(-9)
                    bench_pi_polyfeatures = @benchmark inclusion_prob($Lp)
                    time_get_pi_polyfeatures[nn,dd,mm] = StatsBase.median(bench_pi_polyfeatures.times)*10^(-9)
                end
            end
            save("results/computation_times_d=$(d)_n=$(n).jld", "time_Lr", time_Lr, "time_sample_mDPP", time_sample_mDPP, "time_pi_mDPP", time_pi_mDPP, "time_sample_unif", time_sample_unif, "time_get_sensi", time_get_sensi, "time_sample_sensi", time_sample_sensi, "time_sample_Dsquare", time_sample_Dsquare, "time_compute_polyfeatures", time_compute_polyfeatures, "time_sample_polyfeatures", time_sample_polyfeatures, "time_get_pi_polyfeatures", time_get_pi_polyfeatures, "time_compute_VdM", time_compute_VdM, "time_rff", time_rff, "N", N, "M", M, "D", D)
        end
    end

end

"""
    X, y, theta_gt = generate_lr(d,n,σ_noise)

Generates a random dataset (X, y) for linear regression. X is drawn uniformly
in [0,1]. theta_gt (theta ground truth) as well.

## Inputs
    d the dimension
    n the number of points to generate
    σ_noise the variance of the additional Gaussian noise.

## Outputs
    X the points
    y their value
    theta_gt the ground truth

## Examples

```
n = 1000
σ_noise = 0.1
d = 1;
X, y, theta_gt = generate_lr(d,n,σ_noise)
Plots.scatter(X',y,alpha=.5, label="data")
Plots.plot!(X', X' * theta_gt, line=(:solid,4), color=:black, label="ground truth")
```

"""
function generate_lr(d,n,σ_noise)
    X = rand(Float64,(d,n))
    theta_gt = rand(d)
    y = X' * theta_gt + randn(Float64, n) .* σ_noise
    return X, y, theta_gt
end

"""
    X, ground_truth = generate_GMM(μ, Σ, p_comp, n, q)

Generates a Gaussian mixture X.

## Inputs
    μ the means of each cluster
    Σ the covariance matrices of each cluster
    p_comp the normalized sizes of each cluster (should add up to 1)
    n the number of points to generate
    q the percentage of outliers (generated uniformly in the [-10,10] hypercube)

## Outputs
    X the points
    ground_truth: ground_truth[i] is the number of the cluster point i belongs to. =0 if outlier.

## Examples

```
n = 1000
d = 2;
n_comp = 4;
q = 0.01
p_comp = Vector([0.25; 0.01; 0.54; 0.2])
μ = zeros(Float64, (n_comp, d));
μ[1,:] = [0 0];
μ[2,:] = [10 0];
μ[3,:] = [5 5];
μ[4,:] = [-6 3]
Σ = zeros(Float64, (n_comp, d, d));
Σ[1,:,:] = [1.0 0; 0 1.2];
Σ[2,:,:] = [1.0 0.0; 0.3 1.0];
Σ[3,:,:] = [0.5 0; 0.5 2.0];
Σ[4,:,:] = [1 0; 1.5 2.0]
X, ground_truth = generate_GMM(μ, Σ, p_comp, n, q)
Plots.scatter(X[1,:],X[2,:],color=ground_truth.+1,alpha=.5, label="")
```

"""
function generate_GMM(μ, Σ, p_comp, n, q)
    n_comp = length(p_comp)
    d = size(μ,2)
    n_outliers = Int64(ceil(n*q))
    n = n - n_outliers

    aux = StatsBase.sample(1:n_comp, StatsBase.weights(p_comp), n; replace=true)
    N_comp = zeros(Int64,n_comp)
    for i=1:n_comp
        N_comp[i] = sum(aux.==i)
    end
    X = randn(N_comp[1],d) * Σ[1,:,:] .+ μ[1,:]'
    for nn=2:n_comp
        X = [X; randn(N_comp[nn],d) * Σ[nn,:,:] .+ μ[nn,:]']
    end
    ground_truth = sort(aux)

    X = X';
    playground_min = ones(d) .* -10
    playground_max = ones(d) .* 10
    #playground_min = StatsBase.minimum(X, dims=2)
    #playground_max = StatsBase.maximum(X, dims=2)
    X_outlier = playground_min .+ rand(d, n_outliers) .* (playground_max-playground_min)
    X = [X X_outlier]
    ground_truth = [ground_truth; zeros(Int64, n_outliers, 1)]

    X = X .- StatsBase.mean(X, dims=2)
    return X, ground_truth
end


function sample_for_coresets(method::String, X::Array{Float64}, m::Int64, τ::Float64, r::Int64, problem::String, n_comp::Int64)
    (d, n) = size(X)
    ind_iid = NaN
    if method == "approx_sensitivity"
        s = get_approx_sensitivity_km(X, n_comp)
        #we have the sensitivity upper bounds.
        #We sample iid:
        ind = StatsBase.sample(1:n, StatsBase.weights(s./sum(s)), m; replace=true)
        pi = (s ./ sum(s)) .*m
    else
        error("this method is not yet implemented")
    end
    return ind, pi, ind_iid
end

"""
    ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)

Generates a coreset ind of size m for dataset X with the method specified.
See demo.jl file for more explanations.

For the method approx sensitivity, ncomp is needed. Use
    sample_for_coresets(method::String, X::Array{Float64}, m::Int64, τ::Float64, r::Int64, problem::String, n_comp::Int64)

## Inputs
    method a String specifyingthe desired method
    X the dataset
    m the number of desired samples
    τ the Gaussian parameter (useful only for the methods m-DPP_true_kernel and m-DPP_rff; set to any Float64 otherwise)
    r the number of Random Fourier Features (RFFs). Useful only for the method m-DPP_rff; set to any Int64 otherwise
    problem a String specifying the underlying problem for the method exact_sensitivity; set to any String otherwise

## Outputs
    ind the sample
    pi the vector of inclusion probabilities
    ind_iid an iid sample using the inclusion probabilities (is NaN if the method is already iid)

## Examples
    see demo.jl file

"""
function sample_for_coresets(method::String, X::Array{Float64}, m::Int64, τ::Float64, r::Int64, problem::String)
    (d, n) = size(X)
    ind_iid = NaN
    if method == "m-DPP_true_kernel"
        L = gaussker(X, τ) |> FullRankEnsemble
        #rescale!(L, m)
        ind = DPP.sample(L, m) |> collect
        pi = inclusion_prob(L, m)
        if !isnan(pi[1])
            ind_iid = StatsBase.sample(1:n, StatsBase.weights(pi./sum(pi)), m; replace=true)
        end
    elseif method == "m-DPP_rff"
        Lr = rff(X, r, τ) |> LowRankEnsemble
        #rescale!(Lr, m)
        ind = DPP.sample(Lr, m) |> collect
        pi = inclusion_prob(Lr, m)
        if !isnan(pi[1])
            ind_iid = StatsBase.sample(1:n, StatsBase.weights(pi./sum(pi)), m; replace=true)
        end
    elseif method == "VdM_projective"
        deg=0
        while binomial(deg+d, deg)<m
            #global deg
            deg += 1
        end
        if binomial(deg+d, deg) != m
            @warn "cannot sample with polynomials with these choices of d and m. The closest choices of m around m=$m are m=$(binomial(deg-1+d, deg-1)) or m=$(binomial(deg+d, deg))"
            ind = NaN
            pi = NaN
        else
            Lp = polyfeatures(X, deg) |> ProjectionEnsemble
            ind = DPP.sample(Lp) |> collect
            pi = inclusion_prob(Lp)
            if !isnan(pi[1])
                ind_iid = StatsBase.sample(1:n, StatsBase.weights(pi./sum(pi)), m; replace=true)
            end
        end
    elseif method == "uniform"
        pi = ones(n) .* (1/n) .*m
        ind = StatsBase.sample(1:n, m; replace=true)
    elseif method == "exact_sensitivity"
        if problem == "1-means"
            energy = sum(X.^2, dims=1)
            v = sum(energy) / n
            sigma = (1/n) .* ( 1 .+ energy./v)
            ind = StatsBase.sample(1:n, StatsBase.weights(sigma./sum(sigma)), m; replace=true)
            pi = sigma ./ sum(sigma) .*m
            if !isapprox(sum(sigma), 2, atol=1e-3)
                @warn "sum of sigma is not equal to 2"
            end
            #print("sum(sigma)=$(sum(sigma))")
        elseif problem == "linear-regression"
            d = d-1
            y = X[end,:]
            X = X[1:end-1, :]
            H = X * X'
            H_inv = inv(H)
            ystar = X' * H_inv * X * y
            sigma = zeros(Float64, n)
            for i=1:n
                sigma[i] = X[:,i]' * H_inv * X[:,i] + (y[i]-ystar[i])^2 / (sum((y .- ystar).^2))
            end
            ind = StatsBase.sample(1:n, StatsBase.weights(sigma./sum(sigma)), m; replace=true)
            pi = (sigma ./ sum(sigma)) .*m
            if !isapprox(sum(sigma), d+1, atol=1e-3)
                @warn "sum of sigma is not equal to d+1"
            end
            #print("sum(sigma)=$(sum(sigma))")
        end
    elseif method =="D-square"
        ind, _ = D_square(X, m)
        pi = NaN
    else
        error("this method is not yet implemented")
    end
    if !isnan(pi[1])
        if !isapprox(sum(pi), m, atol=1e-3)
            print("sum(pi)=", sum(pi), " and m=", m)
            error("there is a problem: sum(pi) is not equal to m")
        end
    end
    return ind, pi, ind_iid
end

"""
    s = get_approx_sensitivity_km(X, n_comp)

Bi-criteria approximation to find upper bounds of the sensitivity in the k-means case.

## Inputs
    X the data
    n_comp the number of components (aka k)

## Outputs
    s the vector of upper bounds

## Examples
```
n = 1000
d = 2;
n_comp = 2;
q = 0.01
p_comp = Vector([0.85; 0.15])
μ = zeros(Float64, (n_comp, d));
μ[1,:] = [0 0];
μ[2,:] = [5 5];
Σ = zeros(Float64, (n_comp, d, d));
Σ[1,:,:] = [1.0 0; 0 1.2];
Σ[2,:,:] = [1.0 0.0; 0 1.0];
X, ground_truth = generate_GMM(μ, Σ, p_comp, n, q)
s = get_approx_sensitivity_km(X, n_comp)
```

"""
function get_approx_sensitivity_km(X, n_comp)
    (d, n) = size(X)
    log1surDelta = 10 #do 10 times D^2 to keep the best
    α = 16*(log(n_comp)+2)

    Bs = zeros(Int64, (n_comp, log1surDelta))
    costs = zeros(Float64, log1surDelta)
    for i in 1:log1surDelta
        Bs[:,i], costs[i] = D_square(X, n_comp)
    end
    best = argmin(costs)
    B = Bs[:, best]
    cost = costs[best]

    tr = BruteTree(X[:,B])

    idx,dst = knn(tr,X,1)
    idx = reduce(vcat,idx)
    dst = reduce(vcat,dst).^2

    s = zeros(n)
    mdist  =  group_mean(dst,idx) #returns dict. of tuples (group count,mean dist)
    #compute upper bound on sensitivity
    for i in 1:n
        tmp = (1/cost)*(α*dst[i] + 2*α*mdist[idx[i]][2])
        s[i] = tmp + 4*n/mdist[idx[i]][1]
    end
    return s
end

"""
    group_mean

Useful for the function get_approx_sensitivity_km
"""
function group_mean(v :: Array{T,1}, g :: Array{Int64,1}) where T
    dd = Dict{T,Tuple{Int64,T}}()
    #vv = spzeros(T,length(v))
    for i in 1:length(v)
        if (haskey(dd,g[i]))
            dd[g[i]] = (dd[g[i]][1]+1,dd[g[i]][2]+v[i])
        else
            dd[g[i]] = (1,v[i])
        end
    end
    #@show keys(dd)
    for k in keys(dd)
        dd[k] = (dd[k][1],dd[k][2]/dd[k][1])
    end
    return dd
end


"""
    ind, di = D_square(X, m)

The D^2 sampling method

## Inputs
    X the data
    m the number of desired samples

## Outputs
    ind the samples
    di the total sum of distances between all datapoints and their closest sample in ind

## Examples
```
n = 1000
d = 2;
n_comp = 2;
q = 0.01
p_comp = Vector([0.85; 0.15])
μ = zeros(Float64, (n_comp, d));
μ[1,:] = [0 0];
μ[2,:] = [5 5];
Σ = zeros(Float64, (n_comp, d, d));
Σ[1,:,:] = [1.0 0; 0 1.2];
Σ[2,:,:] = [1.0 0.0; 0 1.0];
X, ground_truth = generate_GMM(μ, Σ, p_comp, n, q)
ind, di = D_square(X, m)
```

"""
function D_square(X, m)
    (d, n) = size(X)
    ind = zeros(Int, m)
    #initial point is sampled unif.
    ind[1] = rand(1:n)
    d2 = colwise(SqEuclidean(), X, X[:,ind[1]])
    for i in 2:m
        ind[i] = StatsBase.sample(Weights(d2))
        dd = colwise(SqEuclidean(),X,X[:,ind[i]])
        d2 = min.(dd,d2)
    end
    return ind, sum(d2)
end


"""
    cost = compute_km_cost(X, thetas, n_comp, n_theta)
    or
    cost = compute_km_cost(X, thetas, n_comp, n_theta, pi)

The first option is to compute the k-means cost for dataset X and centroids in thetas
The second option is to compute the k-means weighted estimated cost with the sampled data in X and their inclusion proba pi

## Inputs
    X the data
    thetas a set of centroids (could be several sets, in which case n_theta is the number of sets of sets)
    n_comp the number of components (aka k)
    possibly pi: inclusion proba for the weighted cost

## Outputs
    the cost

"""
function compute_km_cost(X::Array{Float64}, thetas::Array{Float64}, n_comp::Int64, n_theta::Int64)
    @assert(size(X,1) == size(thetas,1))
    cost = zeros(Float64, n_theta)
    for i=1:n_theta
        theta_loc = thetas[:, (i-1)*n_comp+1:i*n_comp]
        R = pairwise(SqEuclidean(), X, theta_loc, dims=2)
        d = minimum(R, dims=2)
        cost[i] = sum(d)
    end
    return cost
end

function compute_km_cost(X::Array{Float64}, thetas::Array{Float64}, n_comp::Int64, n_theta::Int64, pi::Array{Float64})
    @assert(size(X,1) == size(thetas,1))
    cost = zeros(Float64, n_theta)
    for i=1:n_theta
        theta_loc = thetas[:, (i-1)*n_comp+1:i*n_comp]
        R = pairwise(SqEuclidean(), X, theta_loc, dims=2)
        d = minimum(R, dims=2)
        cost[i] = sum((1 ./pi).* d)
    end
    return cost
end

"""
    cost = compute_lr_cost(X, y, thetas)
    or
    cost = compute_lr_cost(X, y, thetas, pi)

The first option is to compute the linear regression cost for dataset X and centroids in thetas
The second option is to compute the linear regression weighted estimated cost with the sampled data in X and their inclusion proba pi

## Inputs
    X the data
    y the associated value of each point in X
    thetas a set of possible parameters
    possibly pi: inclusion proba for the weighted cost

## Outputs
    the cost

"""
function compute_lr_cost(X::Array{Float64}, y::Array{Float64}, thetas::Array{Float64})
    @assert(size(X,1) == size(thetas,1))
    return sum((y.- X'*thetas).^2, dims=1)
end

function compute_lr_cost(X::Array{Float64}, y::Array{Float64}, thetas::Array{Float64}, pi::Array{Float64})
    @assert(size(X,1) == size(thetas,1))
    aux = (y .- X'*thetas).^2
    for i=1:length(pi)
        aux[i,:] = (1/pi[i]) .* aux[i,:]
    end
    return sum(aux, dims=1)
end

"""
    V_weights = Voronoi_weights(X, centers)

Computes the Voronoi weights (the number of elements in Voronoi cells) for data X and centers
"""
function Voronoi_weights(X::Array{Float64}, centers::Array{Float64})
    (d, n) = size(X)
    @assert(size(X,1) == size(centers,1))

    V_weights = zeros(Float64, size(centers,2))
    R = pairwise(Euclidean(), X, centers, dims=2)
    for i=1:n
        a = argmin(R[i,:])
        V_weights[a] += 1
    end
    return V_weights
end

"""
    test_km_coreset_vs_S

Test k-means coreset method mDPP RFF for different values of τ
"""
function test_km_coreset_vs_S(n_real, n_theta, n_comp, m_max, μ, Σ, p_comp, n, q, S)
    n_methods = length(S)
    ERR = zeros(Float64, (n_methods, n_real, n_theta, m_max))
    ERR_V = zeros(Float64, (n_methods, n_real, n_theta, m_max))
    method = "m-DPP_rff"; r = 200; problem = "N/A"
    for i=1:n_real
        print("$i \n")
        X, ground_truth = generate_GMM(μ, Σ, p_comp, n, q)
        thetas = X[:,rand(1:n, n_theta * n_comp)]
        true_cost = compute_km_cost(X, thetas, n_comp, n_theta)
        for m=1:m_max
            for ss=1:length(S)
                τ = S[ss]
                ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
                if isnan(pi[1])
                    error("pi was not computed")
                end
                est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
                ERR[ss,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
                V_weights = Voronoi_weights(X, X[:, ind])
                est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
                ERR_V[ss,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            end
        end
    end
    return ERR, ERR_V
end

"""
    test_km_coreset

Test k-means coreset methods
"""
function test_km_coreset(n_real, n_theta, n_comp, m_max, μ, Σ, p_comp, n, q, τ1, τ2)

    n_methods = 6
    ERR = zeros(Float64, (n_methods, n_real, n_theta, m_max))
    ERR_V = zeros(Float64, (n_methods, n_real, n_theta, m_max))

    for i=1:n_real
        print("$i \n")
        X, ground_truth = generate_GMM(μ, Σ, p_comp, n, q)
        thetas = X[:,rand(1:n, n_theta * n_comp)]
        true_cost = compute_km_cost(X, thetas, n_comp, n_theta)
        for m=1:m_max
            method = "m-DPP_rff"; τ=τ1; r = 200; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            if isnan(pi[1])
                error("pi was not computed")
            end
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[1,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(X, X[:, ind])
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
            ERR_V[1,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "m-DPP_rff"; τ=τ2; r = 200; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            if isnan(pi[1])
                error("pi was not computed")
            end
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[2,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(X, X[:, ind])
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
            ERR_V[2,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "uniform"; τ = -1.; r = -1; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[3,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(X, X[:, ind])
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
            ERR_V[3,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "exact_sensitivity"; τ = -1.; r = -1; problem = "1-means"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
            ERR[4,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(X, X[:, ind])
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
            ERR_V[4,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "D-square"; τ = -1.; r = -1; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            V_weights = Voronoi_weights(X, X[:, ind])
            est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
            ERR_V[5,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            ERR[5,i,:,m] = ones(Float64,n_theta) .* NaN

            method = "VdM_projective"; τ = -1.; r = -1; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, X, m, τ, r, problem)
            if !isnan(ind[1])
                est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, pi[ind])
                ERR[6,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
                V_weights = Voronoi_weights(X, X[:, ind])
                est_cost = compute_km_cost(X[:, ind], thetas, n_comp, n_theta, 1 ./V_weights)
                ERR_V[6,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            else
                ERR[6,i,:,m] = ones(Float64,n_theta) .* NaN
                ERR_V[6,i,:,m] = ones(Float64,n_theta) .* NaN
            end
        end
    end
    return ERR, ERR_V
end

"""
    test_lr_coreset_vs_S

Test linear regression coreset method mDPP RFF for different values of τ
"""
function test_lr_coreset_vs_S(n_real, n_theta, m_max, d, n, σ_noise, S)

    n_methods = length(S)
    ERR = zeros(Float64, (n_methods, n_real, n_theta, m_max))
    ERR_V = zeros(Float64, (n_methods, n_real, n_theta, m_max))

    method = "m-DPP_rff"; r = 200; problem = "N/A"

    for i=1:n_real
        print("$i \n")
        X, y, theta_gt = generate_lr(d,n,σ_noise)
        Z = [X;y']
        thetas = rand(d, n_theta)
        true_cost = compute_lr_cost(X,y,thetas)
        for m=1:m_max
            for ss=1:length(S)
                τ=S[ss]
                ind, pi, ~ = sample_for_coresets(method, Z, m, τ, r, problem)
                if isnan(pi[1])
                    error("pi was not computed")
                end
                est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
                ERR[ss,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
                V_weights = Voronoi_weights(Z, Z[:, ind])
                est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,1 ./V_weights)
                ERR_V[ss,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            end
        end
    end
    return ERR, ERR_V
end

"""
    test_lr_coreset

Test linear regression coreset methods
"""
function test_lr_coreset(n_real, n_theta, m_max, d, n, σ_noise, τ1, τ2)

    n_methods = 6
    ERR = zeros(Float64, (n_methods, n_real, n_theta, m_max))
    ERR_V = zeros(Float64, (n_methods, n_real, n_theta, m_max))

    for i=1:n_real
        print("$i \n")
        X, y, theta_gt = generate_lr(d,n,σ_noise)
        thetas = rand(d, n_theta)
        true_cost = compute_lr_cost(X,y,thetas)
        Z = [X;y']
        for m=1:m_max
            method = "m-DPP_rff"; τ=τ1; r = 200; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, Z, m, τ, r, problem)
            if isnan(pi[1])
                error("pi was not computed")
            end
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
            ERR[1,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(Z, Z[:, ind])
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,1 ./V_weights)
            ERR_V[1,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "m-DPP_rff"; τ=τ2; r = 200; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, Z, m, τ, r, problem)
            if isnan(pi[1])
                error("pi was not computed")
            end
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
            ERR[2,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(Z, Z[:, ind])
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,1 ./V_weights)
            ERR_V[2,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "uniform"; τ = -1.; r = -1; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, Z, m, τ, r, problem)
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
            ERR[3,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(Z, Z[:, ind])
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,1 ./V_weights)
            ERR_V[3,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "exact_sensitivity"; τ = -1.; r = -1; problem = "linear-regression"
            ind, pi, ~ = sample_for_coresets(method, Z, m, τ, r, problem)
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
            ERR[4,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            V_weights = Voronoi_weights(Z, Z[:, ind])
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,1 ./V_weights)
            ERR_V[4,i,:,m] = abs.(true_cost - est_cost) ./ true_cost

            method = "D-square"; τ = -1.; r = -1; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, Z, m, τ, r, problem)
            V_weights = Voronoi_weights(Z, Z[:, ind])
            est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,1 ./V_weights)
            ERR_V[5,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            ERR[5,i,:,m] = ones(Float64,(n_theta)) .* NaN

            method = "VdM_projective"; τ = -1.; r = -1; problem = "N/A"
            ind, pi, ~ = sample_for_coresets(method, Z, m, τ, r, problem)
            if !isnan(ind[1])
                est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,pi[ind])
                ERR[6,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
                V_weights = Voronoi_weights(Z, Z[:, ind])
                est_cost = compute_lr_cost(X[:,ind],y[ind],thetas,1 ./V_weights)
                ERR_V[6,i,:,m] = abs.(true_cost - est_cost) ./ true_cost
            else
                ERR[6,i,:,m] = ones(Float64,(n_theta)) .* NaN
                ERR_V[6,i,:,m] = ones(Float64,(n_theta)) .* NaN
            end
        end
    end
    return ERR, ERR_V
end

end

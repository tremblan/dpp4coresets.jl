@testset "tests" begin
	n = 30
    X = randn(2,n)
    m = 6;
	method = "m-DPP_true_kernel";
	τ = 5.; r = -1; problem = "N/A";
	ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
	@test isapprox(sum(pi), m)
	@test length(ind) == m
	method = "m-DPP_rff";
	τ = 5.; r = 2*m; problem = "N/A";
	ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
	@test isapprox(sum(pi), m)
	@test length(ind) == m
	method = "VdM_projective";
	τ = -1.; r = -1; problem = "N/A";
	ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
	@test isapprox(sum(pi), m)
	@test length(ind) == m
	method = "uniform";
	τ = -1.; r = -1; problem = "N/A"
	ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
	@test isapprox(sum(pi), m)
	@test length(ind) == m
	method = "exact_sensitivity"; problem = "1-means";
	τ = -1.; r = -1;
	ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
	@test isapprox(sum(pi), m)
	@test length(ind) == m
	method = "exact_sensitivity"; problem = "linear-regression";
	τ = -1.; r = -1;
	ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
	@test isapprox(sum(pi), m)
	@test length(ind) == m
	method = "D-square";
	τ = -1.; r = -1; problem = "N/A"
	ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem)
	@test length(ind) == m
	method = "approx_sensitivity";
	k = 1; τ = -1.; r = -1; problem = "N/A";
	ind, pi, ind_iid = sample_for_coresets(method, X, m, τ, r, problem, k)
	@test isapprox(sum(pi), m)
	@test length(ind) == m
	V_weights = Voronoi_weights(X, X[:, ind])
	@test sum(V_weights) == n
end

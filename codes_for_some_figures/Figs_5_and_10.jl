using DPP4Coresets
cd("/path/to/codes_for_some_figures")

###==== 1 means
n_real=1000;
routine_1_means(n_real)

###==== lr

n_real = 1000;
routine_lr(n_real)

###==== plot results

using Plots
using StatsBase
using JLD
plotly()

epsilon = 0.1

d = 2;
# For Fig 5 of paper:
#dic = load("results/1means_d=$(d).jld")
# For Fig 10 of paper:
dic = load("results/lr_d=$(d)_V_Z.jld")
ERR = dic["ERR"]
ERR_V = dic["ERR_V"]
m_max = dic["m_max"]
τ1 = dic["τ1"]
τ2 = dic["τ2"]
n_theta = dic["n_theta"]

P_success = sum(ERR .< epsilon, dims=3)/n_theta; P_success = P_success[:,:,1,:]
P_success_V = sum(ERR_V .< epsilon, dims=3)/n_theta; P_success_V = P_success_V[:,:,1,:]
P_success_m = StatsBase.mean(P_success, dims=2); P_success_m = P_success_m[:,1,:]
P_success_V_m = StatsBase.mean(P_success_V, dims=2); P_success_V_m = P_success_V_m[:,1,:]

# Figure importance sampling
begin
    m_Vdm = findall(P_success_m[end,:] .!= 0)
    label_str = ["m-DPP (τ=$τ1)" "m-DPP (τ=$τ2)" "Proj Poly DPP" "sensitivity iid" "uniform iid"]

    plot(1:m_max, P_success_m[1,:], line=(:solid,4), label=label_str[1])
    plot!(1:m_max, P_success_m[2,:], line=(:solid,4), label=label_str[2])
    plot!(1:m_max, P_success_m[4,:], line=(:dash,4), label=label_str[4])
    plot!(1:m_max, P_success_m[3,:], line=(:dot,4), label=label_str[5])
    plot!(m_Vdm, P_success_m[end,m_Vdm],  line=(:solid,4), m=(10, :auto), label=label_str[3])

    plot!(framestyle=:zerolines, xlabel = "number of samples m", ylabel = "P(ΔL < εL)")
    plot!(legendfontsize=16, xlims = (0,m_max),ylims = (0,1))
    plot!(yticks = [0,0.5,1], tickfontsize=16, guidefontsize=16)
end

# Figure Voronoi
begin
    m_Vdm = findall(P_success_V_m[end,:] .!= 0)
    label_str_V = ["m-DPP (τ=$τ1)" "m-DPP (τ=$τ2)" "uniform iid" "sensitivity iid" "D^2" "Proj Poly DPP"]

    plot(1:m_max, P_success_V_m[1,:], line=(:solid,4), label=label_str_V[1])
    plot!(1:m_max, P_success_V_m[2,:], line=(:solid,4), label=label_str_V[2])
    plot!(1:m_max, P_success_V_m[4,:], line=(:dash,4), label=label_str_V[4])
    plot!(1:m_max, P_success_V_m[3,:], line=(:dot,4), label=label_str_V[3])
    plot!(m_Vdm, P_success_V_m[end,m_Vdm],  line=(:solid,4), m=(10, :auto), label=label_str_V[end])
    plot!(1:m_max, P_success_V_m[5,:], line=(:dashdot,4), label=label_str_V[5])

    plot!(framestyle=:zerolines, xlabel = "number of samples m", ylabel = "P(ΔL < εL)")
    plot!(legendfontsize=16, xlims = (0,m_max),ylims = (0,1))
    plot!(yticks = [0,0.5,1], tickfontsize=16, guidefontsize=16)
end

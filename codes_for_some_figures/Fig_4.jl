using DPP4Coresets
# This is for Fig. 4

cd("/path/to/codes_for_some_figures")
###==== study 1 means vs τ
n_real = 1000
routine_1_means_vs_S(n_real)

###==== plot results

using Plots
using StatsBase
using JLD
plotly() #or whichever backend you prefer

epsilon=0.1;

d = 2; # or d = 20 or d = 100
dic = load("results/study_1_means_vs_S_d=$(d).jld")
ERR = dic["ERR"]; ERR_V = dic["ERR_V"]
m_max = dic["m_max"]; S = dic["S"]
n_theta = dic["n_theta"]

P_success = sum(ERR .< epsilon, dims=3)/n_theta; P_success = P_success[:,:,1,:]
P_success_V = sum(ERR_V .< epsilon, dims=3)/n_theta; P_success_V = P_success_V[:,:,1,:]
P_success_m = StatsBase.mean(P_success, dims=2); P_success_m = P_success_m[:,1,:]
P_success_V_m = StatsBase.mean(P_success_V, dims=2); P_success_V_m = P_success_V_m[:,1,:]

label_str = "m-DPP (τ=$(S[1]))"
for ss=2:length(S)
    global label_str
    label_str = [label_str "m-DPP (τ=$(S[ss]))"]
end

label_str=label_str[:,1:end]
P_success_m = P_success_m[1:end,:]
P_success_V_m = P_success_V_m[1:end,:]
# Figure importance sampling
begin
    plot(1:m_max, P_success_m', line=(:auto,4), label=label_str)

    plot!(framestyle=:zerolines, xlabel = "number of samples m", ylabel = "P(ΔL < εL)")
    plot!(legendfontsize=16, xlims = (0,m_max),ylims = (0,1))
    plot!(yticks = [0,0.5,1], tickfontsize=16)
    plot!(guidefontsize=16)
end

# Figure Voronoi
begin
    plot(1:m_max, P_success_V_m', line=(:auto,4), label=label_str)

    plot!(framestyle=:zerolines, xlabel = "number of samples m", ylabel = "P(ΔL < εL)")
    plot!(legendfontsize=16, xlims = (0,m_max),ylims = (0,1))
    plot!(yticks = [0,0.5,1], tickfontsize=16, guidefontsize=16)
end

using DPP4Coresets
#This if for Fig. 9

cd("/path/to/codes_for_some_figures")
###==== study linear regression vs τ
n_real = 1000
routine_lr_vs_S(n_real)

###==== plot results

using Plots
using StatsBase
using JLD
plotly()

epsilon = 0.1

d = 2; # or d = 20
dic = load("results/study_lr_vs_S_d=$(d)_V_Z.jld")
ERR = dic["ERR"]; ERR_V = dic["ERR_V"]
m_max = dic["m_max"]; S = dic["S"]
n_theta = dic["n_theta"]

P_success = sum(ERR .< epsilon, dims=3)/n_theta; P_success = P_success[:,:,1,:]
P_success_V = sum(ERR_V .< epsilon, dims=3)/n_theta; P_success_V = P_success_V[:,:,1,:]
P_success_m = StatsBase.mean(P_success, dims=2); P_success_m = P_success_m[:,1,:]
P_success_V_m = StatsBase.mean(P_success_V, dims=2); P_success_V_m = P_success_V_m[:,1,:]

label_str = "mDPP τ=$(S[1])"
for ss=2:length(S)
    global label_str
    label_str = [label_str "mDPP τ=$(S[ss])"]
end

label_str=label_str[:,3:end-2]
P_success_m = P_success_m[3:end-2,:]
P_success_V_m = P_success_V_m[3:end-2,:]

# Figure importance sampling
begin
    plot(1:m_max, P_success_m', line=(:auto,4), label=label_str)

    plot!(framestyle=:zerolines, xlabel = "number of samples m", ylabel = "P(ΔL < εL)")
    plot!(legendfontsize=16, xlims = (0,m_max),ylims = (0,1))
    plot!(yticks = [0,0.5,1], tickfontsize=16)
    plot!(guidefontsize=16)
end

# Figure Voronoi
begin
    plot(1:m_max, P_success_V_m', line=(:auto,4), label=label_str)

    plot!(framestyle=:zerolines, xlabel = "number of samples m", ylabel = "P(ΔL < εL)")
    plot!(legendfontsize=16, xlims = (0,m_max),ylims = (0,1))
    plot!(yticks = [0,0.5,1], tickfontsize=16, guidefontsize=16)
end
